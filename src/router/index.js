import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/components/home/Home'
import TransaksiIndex from '@/components/transaksi/TransaksiIndex'
import RekapIndex from '@/components/transaksi/RekapIndex'
import Login from '@/components/auth/Login'

Vue.use(VueRouter)

export default new VueRouter({
  history: true,
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
      meta: {auth: false}
    },
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {auth: true}
    },
    {
      path: '/transaksi',
      name: 'TransaksiIndex',
      component: TransaksiIndex,
      meta: {auth: true}
    },
    {
      path: '/rekap',
      name: 'RekapIndex',
      component: RekapIndex,
      meta: {auth: true}
    }
  ]
})
