// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'es6-promise/auto'
import 'vue-loading-overlay/dist/vue-loading.css' // loading css
import axios from 'axios'
import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import App from './App'
import auth from './auth'
import router from './router'
import vuetify from '@/plugins/vuetify' // path to vuetify export
import Loading from 'vue-loading-overlay'
import myUpload from 'vue-image-crop-upload'
import Vue2Editor from 'vue2-editor'
import VueSimpleAlert from 'vue-simple-alert'

// Set Vue globally
window.Vue = Vue
// Set Vue router
Vue.router = router
Vue.use(VueRouter)

Vue.use(VueAxios, axios)
axios.defaults.baseURL = process.env.API_ENDPOINT + '/api'
Vue.axios.defaults.headers.common['Access-Control-Allow-Origin'] = true
Vue.axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*'
Vue.axios.defaults.headers.common['Accept'] = 'application/json, text/plain, */*'
Vue.axios.defaults.headers.common['Content-Type'] = 'application/json;charset=UTF-8'
Vue.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
Vue.axios.defaults.headers.common['Access-Control-Expose-Headers'] = 'https://developer.mozilla.org'
Vue.axios.defaults.headers.common['Cache-Control'] = 'no-cache'
Vue.axios.defaults.headers.common['Content-Language'] = 'en-US, de-DE, en-CA'
// Vue.axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name='csrf-token']').getAttribute('content');
// Vue.axios.defaults.headers.common['Expires'] = 'Wed, 31-Dec-2008 01:34:53 GMT'
// Vue.axios.defaults.headers.common['Last-Modified'] = 'Wed, 21 Oct 2015 07:28:00 GMT'
// Vue.axios.defaults.headers.common['Pragma'] = 'no-cache'
// Vue.axios.defaults.headers.common['bearer'] = 'no-cache'
Vue.prototype.$axios = axios

// Set Vue authentication
Vue.use(VueAuth, auth)

// Init plugin loading
Vue.use(Loading, {
  // props
  zIndex: 100001,
  color: '#f44336',
  loader: 'dots'
  // is-full-page:true,
})

// text editor
Vue.use(Vue2Editor)

// simple alert
Vue.use(VueSimpleAlert)

Vue.mixin({
  data: function () {
    return {
      get basePath () {
        return process.env.API_ENDPOINT
      }
    }
  }
})

Vue.filter('toCurrency', function (value) {
  if (typeof value !== 'number') {
    return value
  }
  var formatter = new Intl.NumberFormat('id-ID', {
    // style: 'currency',
    currency: 'IDR',
    minimumFractionDigits: 0
  })
  return formatter.format(value)
})

Vue.config.productionTip = false

Vue.component('my-upload', myUpload)

/* eslint-disable no-new */
new Vue({
  vuetify,
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
